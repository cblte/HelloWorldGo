package main

import "testing"

func TestGreet(t *testing.T) {
	// Test with a name of "Alice"
	expected := "Hello, Alice!\n"
	if result := greet("Alice"); result != expected {
		t.Errorf("greet(\"Alice\") returned %q, expected %q", result, expected)
	}

	// Test with a name of "Bob"
	expected = "Hello, Bob!\n"
	if result := greet("Bob"); result != expected {
		t.Errorf("greet(\"Bob\") returned %q, expected %q", result, expected)
	}
}
