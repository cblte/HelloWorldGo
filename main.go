package main

import "fmt"

func greet(name string) string {
	return fmt.Sprintf("Hello, %s!\n", name)
}

func main() {
	fmt.Println(greet("Alice"))
	fmt.Println(greet("Bob"))
}
